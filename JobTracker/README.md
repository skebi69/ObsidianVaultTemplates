## How to use this Vault
1. Clone this repo into the directory where you keep your Obsidian Vaults
2. Open Obsidian
3. Open Vault
4. Choose "JobTracker" folder in your cloned repo
6. Good luck in your job hunt

## Using the tracker
1. In the JobTracker directory create a new note
2. Name the note however you want - I generally call it